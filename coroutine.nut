class BCoroutine {
    _coroutines = []

    static function create(clousure, worker = null) {
        local coro = ::newthread(clousure)
        if (worker) {
            worker.register(coro)
        }

        return coro
    }

    static function hasFinished(coro) {
        return coro.getstatus() == "idle"
    }

    static function sleep(ms, fetch_time = getTickCount) {
        local duration = fetch_time() + ms
        while (getTickCount() < duration) {
            ::suspend(null)
        }
    }

    function waitToFinish(coroutines, context = null) {
        local finished = false
        while (!finished) {
            finished = true

            foreach (coro in coroutines) {
                if (!this.hasFinished(coro)) {
                    coro.wakeup(context)
                    finished = false
                }
            }

            ::suspend(null)
        }
    }

    function waitConditional(condition, context = null) {
        local met = false
        while (!(met = condition(context))) {
            ::suspend(null)
        }
    }
}

//--------------------------------------------

class BWorker {
    _coroutines = null

    constructor() {
        this._coroutines = []
    }

    function register(coro) {
        this._coroutines.push(coro.weakref())
    }

    function wakeup(context = null) {
        local length = this._coroutines.len()
        for (local i = 0; i < length; ++i) {
            local co = this._coroutines[i]
            if (co == null) {
                this._coroutines[i] = this._coroutines[--length]
                this._coroutines.pop()
                continue
            }

            if (!BCoroutine.hasFinished(co)) {
                co.wakeup(context)
            }
        }
    }
}
