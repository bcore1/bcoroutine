# BCoroutine - Utility module for coroutines in Squirrel

```js

local function coro1() {
    print("Test")
    BCoroutine.sleep(200)
    print("Test")
    BCoroutine.sleep(200)
    print("Test")
    BCoroutine.sleep(200)
    BCoroutine.waitConditional(@(_) true)
    print("Test")
}


local worker = BWorker()
local coro = BCoroutine.create(coro1, worker)
coro.call()

while (true) {
    worker.wakeup()
}
```
